#!/bin/bash
#author	:Bruno Maciel Peres
#email	:brunomperes@dcc.ufmg.br

entradas=(
	"12345678_"  # Resolvido
	"1234_6758"  # abaixo, direita
	"1234567_8"  # direita
	"123456_78"  # direita, direita
	"12348675_"  # Sem solucao
) 

for i in "${entradas[@]}"
do
	echo "--------------- Teste de $i"
	./avalia_sucessor.sh $i
	./avalia_expande.sh $i 0
	./avalia_bfs.sh $i
	./avalia_dfs.sh $i
	./avalia_astar_h1.sh $i
	./avalia_astar_h2.sh $i
done
