#!/usr/bin/python

"""
Lista 1 de IA

Para execucao, requer um parametro que define qual questao esta sendo
respondida e um estado. Para a questao 2 eh necessario um terceiro parametro de
 custo inicial.

__author__ = "Bruno Maciel Peres"
__email__ = "brunomperes@dcc.ufmg.br
"""

import sys
from estado import Estado
from algoritmos import graphSearch, Fronteira

# Posicao do vetor na moldura:
#
#   0  |  1  |  2
#   3  |  4  |  5
#   6  |  7  |  8


def imprimeExpande(resultadoExpande):
    """
    Formata o resultado da funcao expande para o esperado na saida
    @return (acao,estado,custo,pai.estado)
    """
    quantidadeFilhos = len(resultadoExpande)
    resultado = ""
    for indice, pos in enumerate(resultadoExpande):
        resultado += '(' + pos.acao + ',' + str(pos) + ',' + str(pos.custo) + \
            ',' + str(pos.pai) + ')'
        if indice != quantidadeFilhos - 1:
            resultado += ' '
    print resultado


def imprimeSucessor(resultadoSucessor):
    quantidadeFilhos = len(resultadoSucessor)
    resultado = ""
    for indice, pos in enumerate(resultadoSucessor):
        resultado += '(' + pos[0] + ',' + pos[1] + ')'
        if indice != quantidadeFilhos - 1:
            resultado += ' '
    print resultado


def main(questao, estadoInicial, custoInicial):
    """Ponto de entrada do script"""
    e = Estado(estadoInicial, custoInicial)
    if questao == '1':
        imprimeSucessor(e.sucessor())
    elif questao == '2':
        imprimeExpande(e.expande())
    elif questao == '3bfs':
        f = Fronteira('fila')
        print graphSearch(f, e)
    elif questao == '3dfs':
        f = Fronteira('pilha')
        print graphSearch(f, e)
    elif questao == '3h1':
        f = Fronteira('h1')
        print graphSearch(f, e)
    elif questao == '3h2':
        f = Fronteira('h2')
        print graphSearch(f, e)

if __name__ == '__main__':
    arg1 = sys.argv[1]  # Questao a ser respondida
    arg2 = ""  # Estado inicial
    arg3 = 0  # Custo inicial
    if len(sys.argv) > 2:
        arg2 = sys.argv[2]
        if len(sys.argv) > 3:
            arg3 = sys.argv[3]
    main(arg1, arg2, arg3)
