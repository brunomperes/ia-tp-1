"""
Implementa a classe Estado e as operacoes que podem ser realizadas sobre ela,
como sucessor, expande, ehSolucao e stringMovimentacao

__author__ = "Bruno Maciel Peres"
__email__ = "brunomperes@dcc.ufmg.br
"""

ESTADO_OBJETIVO = "12345678_"
DIRECOES = ('esquerda', 'abaixo', 'direita', 'acima')
ESPACO = '_'


class Estado(object):
    """
    Representa um estado (no) na arvore de solucoes
    """
    def __init__(self, estadoInicial, custo):
        super(Estado, self).__init__()
        self.estado = list(estadoInicial)
        self.pai = None
        self.acao = ""
        self.custo = int(custo)
        self.filhos = []

    def __str__(self):
        return self.estadoComoString()

    def imprimeMoldura(self):
        representacao = '  ' + self.estado[0] + '  |  ' + self.estado[1] + '  |  ' + self.estado[2] + '\n'
        representacao += '  ' + self.estado[3] + '  |  ' + self.estado[4] + '  |  ' + self.estado[5] + '\n'
        representacao += '  ' + self.estado[6] + '  |  ' + self.estado[7] + '  |  ' + self.estado[8] + '\n'
        print representacao

    def posicaoEspaco(self):
        for indice, caracter in enumerate(self.estado):
            if caracter == "_":
                return indice
        raise Exception('Estado nao contem espaco')

    def pecasForaDoLugar(self):
        pecasFora = 0
        for indice, c in enumerate(self.estado):
            if c != ESTADO_OBJETIVO[indice]:
                pecasFora += 1
        return pecasFora

    def permitidoMover(self, direcao):
        pos = self.posicaoEspaco()
        if (pos == 0 or pos == 1 or pos == 2) and direcao == 'acima':
            return False
        if (pos == 0 or pos == 3 or pos == 6) and direcao == 'esquerda':
            return False
        if (pos == 6 or pos == 7 or pos == 8) and direcao == 'abaixo':
            return False
        if (pos == 2 or pos == 5 or pos == 8) and direcao == 'direita':
            return False
        return True

    def sucessor(self):
        """
        Questao 1
        entrada:  self.estado = "2_3541687"
        retorno: (esquerda,_23541687) (abaixo,2435_1687) (direita,23_541687)
        """
        possibilidades = []
        for direcao in DIRECOES:
            possibilidade = self.stringMovimentacao(direcao)
            if (possibilidade is not False):
                possibilidades.append((direcao, possibilidade))
        return possibilidades

    def expande(self):
        """
        Questao 2
        @return Lista de estados das possibilidades de
                expansao de um estado
        """
        for direcao in DIRECOES:
            if (self.permitidoMover(direcao)):
                possibilidade = self.stringMovimentacao(direcao)
                novoEstado = Estado(possibilidade, self.custo + 1)
                novoEstado.acao = direcao
                novoEstado.pai = self
                self.filhos.append(novoEstado)
        return self.filhos

    def stringMovimentacao(self, direcao):
        """
        Retorna uma string com a movimentacao ocorrida se for viavel a
        movimentacao
        @return "23_541687" se for valida ou False se movimentacao invalida
        """
        novoEstado = Estado(self.estado, self.custo)
        novoEstado = novoEstado.estado
        pos = self.posicaoEspaco()
        if self.permitidoMover(direcao):
            if direcao == 'acima':
                novoEstado[pos] = novoEstado[pos - 3]
                novoEstado[pos - 3] = ESPACO
            elif direcao == 'abaixo':
                novoEstado[pos] = novoEstado[pos + 3]
                novoEstado[pos + 3] = ESPACO
            elif direcao == 'esquerda':
                novoEstado[pos] = novoEstado[pos - 1]
                novoEstado[pos - 1] = ESPACO
            elif direcao == 'direita':
                novoEstado[pos] = novoEstado[pos + 1]
                novoEstado[pos + 1] = ESPACO
        else:
            return False
        return ''.join(novoEstado)

    def estadoComoString(self):
        return ''.join(self.estado)

    def ehSolucao(self):
        if self.estadoComoString() == ESTADO_OBJETIVO:
            return True
        else:
            return False
