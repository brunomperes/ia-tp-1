from collections import deque
from estado import Estado, ESPACO

"""
Implementa as funcionalidades necessarias de fronteira, os algoritmos de
selecao bfs, dfs, h1 e h2 e pesquisa no grafo

__author__ = "Bruno Maciel Peres"
__email__ = "brunomperes@dcc.ufmg.br
"""


class Fronteira():
    """
    Classe Fronteira
    Recebe o nome do algoritmo para implementar fronteira
    BFS -> fila
    DFS -> pilha
    A*h1 -> lista
    A*h2 -> lista
    """
    def __init__(self, algoritmo):
        self.algoritmo = algoritmo
        if self.algoritmo == 'fila':
            self.lista = deque([])
        else:
            # h1, h2 e pilha sao implementados como lista
            self.lista = []

    def insere(self, element):
        self.lista.append(element)

    def insere_lista(self, lista):
        """Insere uma lista de Estados"""
        for elemento in lista:
            self.lista.append(elemento)

    def remove(self):
        if self.algoritmo == 'pilha':
            return self.lista.pop()
        elif self.algoritmo == 'fila':
            return self.lista.popleft()
        elif self.algoritmo == 'h1':
            return self.poph1()
        elif self.algoritmo == 'h2':
            return self.poph2()

    def poph1(self):
        """
        Remove e retorna o no da fronteira com menor f(node) = g(node) + h(node)
        g(node) -> node.custo e h(node) -> numero de pecas fora do lugar
        """
        nodeMenorF = None
        indiceMenorF = None
        for indice, elemento in enumerate(self.lista):
            f = elemento.custo
            f += elemento.pecasForaDoLugar()
            elemento.f = f
            if not nodeMenorF or nodeMenorF.f > elemento.f:
                nodeMenorF = elemento
                indiceMenorF = indice
        return self.lista.pop(indiceMenorF)
    
    def poph2(self):
        """
        Remove e retorna o no da fronteira com menor f(node) = g(node) + h(node)
        g(node) -> node.custo e h(node) -> distancia de Manhattan de todas as
        pecas aos seus respectivos lugares
        """
        nodeMenorF = None
        indiceMenorF = None
        for indice, elemento in enumerate(self.lista):
            f = elemento.custo
            for indiceChar, c in enumerate(elemento.estado):
                if c == ESPACO:
                    continue
                f += distanciaManhattan(indiceChar, c)
            elemento.f = f
            if not nodeMenorF or nodeMenorF.f > elemento.f:
                nodeMenorF = elemento
                indiceMenorF = indice
        return self.lista.pop(indiceMenorF)

    def vazia(self):
        return len(self.lista) == 0

    def __str__(self):
        resultado = ""
        for elemento in self.lista:
            resultado += str(elemento) + ', '
        return resultado


def distanciaManhattan(indice, destino):
        """
        Calcula a distancia de Manhattan da origem ao destino em um 8 puzzle
        @params indice: indice em um array de 9 posicoes
                destino: valor do caracter (numero 1 deve ficar na casa 1)
        @return int
        """
        distanciaVertical = abs(int(indice / 3) - int((int(destino) - 1) / 3))
        distanciaHorizontal = abs(int(indice % 3) - int((int(destino) - 1) % 3))
        return distanciaVertical + distanciaHorizontal


def graphSearch(fronteira, estadoInicial):
    """
    @return string com a lista de acoes para finalizar o estado
    """
    fechado = set()
    fronteira.insere(Estado(estadoInicial.estado, estadoInicial.custo))
    while True:
        if fronteira.vazia():
            return "Sem solucao"

        e = fronteira.remove()
        node = Estado(e.estado, e.custo)
        node.pai = e.pai
        node.acao = e.acao
        if node.ehSolucao():
            iterador = node
            listaDeAcoes = list()
            # Itera na arvore para buscar todas as acoes realizadas
            while iterador.pai is not None:
                listaDeAcoes.append(iterador.acao)
                iterador = iterador.pai
            listaDeAcoes.reverse()
            return '%s' % ' '.join(map(str, listaDeAcoes))

        if stringHash(node.estado) not in fechado:
            fechado.add(stringHash(node.estadoComoString()))
            fronteira.insere_lista(node.expande())


def stringHash(s):
    """Usado como restricao do tipo set de python, que deve ser hashable"""
    ord3 = lambda x: '%.3d' % ord(x)
    return int(''.join(map(ord3, s)))
